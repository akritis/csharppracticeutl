﻿using System;

namespace Unittestingdemo
{
    internal class SuperHero
    {
        public String name { get; set; }
        public String power { get; set; }
        public String brand { get; set; }
        public int age { get; set; }
    }
}