﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace helloworld
{
    class Program
    {
        static void Main(string[] args)
        {
            //create two string variables and assigning them some values. 
            var message = "hello world";
            var message2 = "salman ; being human";
            int x = 3;
            double y = 23.98;
                string m = "hello";
            //building the final output message. and also do some essential formatting so it looks good.
            var outputmessage = message + ".\n" + message2;
            //print the output message.
            Console.WriteLine(outputmessage);
            outputmessage = "yup";
            Console.Write(x);
            Console.Write(y);
            Console.Write(m);


            //put this so that the program will not exit and keep the output window. 
            //the moment any key is pressed, the program will exist. 
            Console.ReadLine();
        }
    }
}
