﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvcdemo5.Startup))]
namespace mvcdemo5
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
