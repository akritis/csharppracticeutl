﻿using MVCDemo3.Models;
using mvcDemo4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult ShowList()
        {
            ViewBag.Message = "Your showlist page.";

            var ListOfSuperHeroes = new List<SuperHero>();

            ListOfSuperHeroes = GetFiveSuperHeroes();


            //return View(db.NewsModel.ToList());
            return View(ListOfSuperHeroes);
        }
        public ActionResult ShowList1()
        {
            ViewBag.Message = "Your showlist page.";

            var ListOfStudents = new List<Students>();

            ListOfStudents = GetFiveStudents();


            //return View(db.NewsModel.ToList());
            return View(ListOfStudents);
        }

        private List<Students> GetFiveStudents()
        {
            var tempList1 = new List<Students>();

            //five super heroes. 
            var name = "";
            var roll = "";

            //five super heroes. 
            var tempstud = new Students();
            var tempstud1 = new Students();
            var tempstud2 = new Students();
            var tempstud3 = new Students();
            var tempstud4 = new Students();
            var tempstud5 = new Students();

            name = "Batman";
            roll = "24";

            tempstud.name = name;
            tempstud.roll = roll;
            //add this hero to list. 
            tempList1.Add(tempstud);


            name = "antman";
            roll = "23";

            tempstud1.name = name;
            tempstud1.roll = roll;

            //add this hero to list. 
            tempList1.Add(tempstud1);

            name = "atman";
            roll = "25";

            tempstud2.name = name;
            tempstud2.roll = roll;

            //add this hero to list. 
            tempList1.Add(tempstud2);

            name = "Batman";
            roll = "26";

            tempstud3.name = name;
            tempstud3.roll = roll;

            //add this hero to list. 
            tempList1.Add(tempstud3);

            name = "Bateman";
            roll = "27";

            tempstud4.name = name;
            tempstud4.roll = roll;
            //add this hero to list. 
            tempList1.Add(tempstud4);

            name = "Batman";
            roll = "28";

            tempstud5.name = name;
            tempstud5.roll = roll;
            

            //add this hero to list. 
            tempList1.Add(tempstud5);


            return tempList1;

        }

        //we will display this in the view
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}