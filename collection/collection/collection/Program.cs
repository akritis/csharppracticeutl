﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();

            
            var ListOfSuperHeroes2 = new List<SuperHero>();
            var tempHero1 = new SuperHero();
            tempHero1.age = 20;
            tempHero1.brand = "DC";
            tempHero1.name = "ONE";
            tempHero1.power = "MONEY";
            ListOfSuperHeroes = GetFiveSuperHeroes();
            var tempHero2 = new SuperHero();
            tempHero2.age = 30;
            tempHero2.name = "two";
            tempHero2.brand = "dc";
            tempHero2.power = "money";
            ListOfSuperHeroes2.Add(tempHero1);
            ListOfSuperHeroes2.Add(tempHero2);
           // ShowSuperHeroes(ListOfSuperHeroes);
            //ShowSuperHeroes(ListOfSuperHeroes2);
            var FinalList = new List<SuperHero>();
            FinalList.AddRange(ListOfSuperHeroes);
            FinalList.AddRange(ListOfSuperHeroes2);
            //ShowSuperHeroes(FinalList);
            int age = 10;
            String Name = "Batman";
            SuperHero returnedHero = findByAge(age , FinalList);
            SuperHero returnedname = findByname(Name, FinalList);
            Console.ReadLine();
        }

        private static SuperHero findByname(object name, List<SuperHero> finalList)
        {
            SuperHero temphero1 = new SuperHero();
            temphero1 = finalList.Select(x => x).Where(x => x.name == name).FirstOrDefault();
            return temphero1;
        }

        private static SuperHero findByAge(int age, List<SuperHero> finalList)
        {
            SuperHero temphero = new SuperHero();
            //var temphero = listOfSuperHeroes.Select(x => x).Where(x => x.age == 10);
            temphero = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();
            return temphero;
        }

        

        private static void ShowSuperHeroes(List<SuperHero> listOfSuperHeroes)
        {
            //.OrderByDescending(x => x.Delivery.SubmissionDate);
            //istOfSuperHeroes.OrderByDescending(listOfSuperHeroes => listOfSuperHeroes.age);
            listOfSuperHeroes = listOfSuperHeroes.OrderByDescending(x => x.age).ToList();

            foreach (var x in listOfSuperHeroes)
            {
                Console.WriteLine("------------------");
                Console.WriteLine("Name " + x.name);
                Console.WriteLine("Age " + x.age);
                Console.WriteLine("Power  " + x.power);
                Console.WriteLine("Brand " + x.brand);
                Console.WriteLine("------------------");
            }
        }

        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();
           

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 10;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "IRONman";
            power = "Money";
            brand = "DC";
            age =10;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "SUPERman";
            power = "Money";
            brand = "DC";
            age = 28;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 38;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 78;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "CAPTAIN AMERICA";
            power = "STRENGTH";
            brand = "DC";
            age = 18;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }
    }
}
