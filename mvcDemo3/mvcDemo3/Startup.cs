﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvcDemo3.Startup))]
namespace mvcDemo3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
